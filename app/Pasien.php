<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
  protected $table    = 'pasien';
  protected $fillable = ['nama', 'alamat', 'no_telp', 'rt_rw', 'kelurahan_id', 'tanggal_lahir', 'jenis_kelamin'];
  public function kelurahan()
  {
    return $this->belongsTo('App\Kelurahan');
  }
}
