<?php

namespace App\Http\Controllers;

use Datatables;
use Illuminate\Http\Request;
use \App\Kelurahan;
use \App\Pasien;
use \App\User;

class DatatablesController extends Controller
{
  /**
   * Process datatables ajax request.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function users()
  {
    return Datatables::of(User::all())
      ->editColumn('is_admin', '@if($is_admin) Admin @else Operator @endif')
      ->make(true);
  }

  public function kelurahan()
  {
    return Datatables::of(Kelurahan::query())
      ->addColumn('actions', '@can(\'user-admin\')
          <a href="{{ route(\'kelurahan.edit\', $id) }}" class="fa fa-edit btn btn-sm btn-primary">Edit</a>
            {{postLink(route("kelurahan.destroy",$id),"<span class=\'fa fa-trash btn btn-danger btn-sm\'>Delete</span>","Are you sure to delete?")}}
        @endcan')
      ->make(true);
  }

  public function pasien()
  {
    $pasien = Pasien::all();
    foreach ($pasien as $key => $value) {
      if (empty($value->kelurahan)) {
        $value->kel = 'no-data';
      } else {
        $value->kel = $value->kelurahan->kelurahan . ' - ' . $value->kelurahan->kecamatan . ', ' . $value->kelurahan->kota;
      }

    }
    return Datatables::of($pasien)
      ->addColumn('actions', '
          <a href="{{ route(\'pasien.edit\', $id) }}" class="fa fa-edit btn btn-sm btn-primary">Edit</a>
            {{postLink(route("pasien.destroy",$id),"<span class=\'fa fa-trash btn btn-danger btn-sm\'>Delete</span>","Are you sure to delete?")}}')
      ->make(true);
  }
}
