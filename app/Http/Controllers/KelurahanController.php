<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Kelurahan;

class KelurahanController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $kelurahan = Kelurahan::all();
    return view('kelurahan.index', compact('kelurahan'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('kelurahan.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'kelurahan' => 'required',
      'kecamatan' => 'required',
      'kota'      => 'required',
    ]);

    $kelurahan = Kelurahan::create($request->all());

    if ($kelurahan->save()) {
      return redirect()->route('kelurahan.index')->with('flash-success', 'Data berhasil disimpan.');
    }

    return redirect()->back()->withErrors($kelurahan->getErrors())->withInput();return $request->all();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $kelurahan = Kelurahan::find($id);
    return view('kelurahan.edit', compact('kelurahan'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $kelurahan = Kelurahan::find($id);
    $kelurahan->update($request->all());
    return redirect()->route('kelurahan.index')->with('flash-success', 'Data berhasil disimpan.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $kelurahan = Kelurahan::find($id);
    if ($kelurahan->delete()) {
      return redirect()->route('kelurahan.index')->with('flash-success', 'Data berhasil dihapus.');
    } else {
      return redirect()->route('kelurahan.index')->with('flash-errors', 'Data gagal dihapus.');
    }
  }
}
