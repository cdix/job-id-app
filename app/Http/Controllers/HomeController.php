<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    // return \Carbon\Carbon::now()->toDateTimeString();
    // if (\Auth::user()->is_admin) {
    //   return 'ok';
    // }

    return view('home');
  }
}
