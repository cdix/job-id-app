<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Kelurahan;
use \App\Pasien;

class PasienController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $pasiens = Pasien::all();
    return view('pasien.index', compact('pasiens'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $kelurahan = Kelurahan::all();
    return view('pasien.create', compact('kelurahan'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'nama'          => 'required',
      'alamat'        => 'required',
      'no_telp'       => 'required',
      'rt_rw'         => 'required',
      'tanggal_lahir' => 'required',
      'jenis_kelamin' => 'required',
    ]);
    $ym          = \Carbon\Carbon::now()->format('ym');
    $last_pasien = Pasien::orderBy('created_at', 'desc')->first();
    if ($ym == substr($last_pasien->id_pasien, 0, 4)) {
      $new_id = (int) substr($last_pasien->id_pasien, 4) + 1;
      if (strlen($new_id) == 1) {
        $new_id = '00000' . $new_id;
      } elseif (strlen($new_id) == 2) {
        $new_id = '0000' . $new_id;
      } elseif (strlen($new_id) == 3) {
        $new_id = '000' . $new_id;
      } elseif (strlen($new_id) == 4) {
        $new_id = '00' . $new_id;
      } elseif (strlen($new_id) == 5) {
        $new_id = '0' . $new_id;
      }
      $new_id = $ym . $new_id;
    } else {
      $new_id = $ym . '000001';
    }

    $pasien                = new Pasien;
    $pasien->nama          = $request->nama;
    $pasien->alamat        = $request->alamat;
    $pasien->no_telp       = $request->no_telp;
    $pasien->rt_rw         = $request->rt_rw;
    $pasien->kelurahan_id  = $request->kelurahan_id;
    $pasien->tanggal_lahir = $request->tanggal_lahir;
    $pasien->jenis_kelamin = $request->jenis_kelamin;
    $pasien->id_pasien     = (int) $new_id;

    if ($pasien->save()) {
      return redirect()->route('pasien.index')->with('flash-success', 'Data berhasil disimpan.');
    }

    return redirect()->back()->withErrors($pasien->getErrors())->withInput();return $request->all();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $pasien    = Pasien::find($id);
    $kelurahan = Kelurahan::all();
    return view('pasien.edit', compact('pasien', 'kelurahan'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $pasien = Pasien::find($id);
    $pasien->update($request->all());
    return redirect()->route('pasien.index')->with('flash-success', 'Data berhasil disimpan.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $pasien = Pasien::find($id);
    if ($pasien->delete()) {
      return redirect()->route('pasien.index')->with('flash-success', 'Data berhasil dihapus.');
    } else {
      return redirect()->route('pasien.index')->with('flash-errors', 'Data gagal dihapus.');
    }
  }
}
