<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasienTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('pasien', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('id_pasien');
      $table->string('nama');
      $table->text('alamat');
      $table->string('no_telp');
      $table->string('rt_rw');
      $table->date('tanggal_lahir');
      $table->string('jenis_kelamin');
      $table->integer('kelurahan_id')->unsigned();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('pasiens');
  }
}
