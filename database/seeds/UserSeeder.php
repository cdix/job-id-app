<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $users = ['name' => 'sidik', 'email' => 'sidik.saepudin13@gmail.com', 'password' => bcrypt('password'), 'is_admin' => true];
    DB::table('users')->insert($users);
  }
}
