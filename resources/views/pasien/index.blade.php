@extends('adminlte::page')

@section('title', 'Pasien')

@section('content_header')
    <h1>Data Pasien</h1>
@stop

@section('content')
<a href="{{ route('pasien.create') }}" class="btn btn-primary btn-md">Create Pasien</a>
<table class="table table-striped table-hover table-bordered" id="pasien-table">
  <thead>
    <tr>
      <th>ID Pasien</th>
      <th>Nama</th>
      <th>Alamat</th>
      <th>No Telepon</th>
      <th>RT/RW</th>
      <th>Kelurahan</th>
      <th>Tanggal Lahir</th>
      <th>Jenis Kelamin</th>
      <th></th>
    </tr>
  </thead>
</table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@push('js')
<script>
$(function() {
    $('#pasien-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.pasien') !!}',
        columns: [
            { data: 'id_pasien', name: 'id_pasien' },
            { data: 'nama', name: 'nama' },
            { data: 'alamat', name: 'alamat' },
            { data: 'no_telp', name: 'no_telp' },
            { data: 'rt_rw', name: 'rt_rw' },
            { data: 'kel', name: 'kel' },
            { data: 'tanggal_lahir', name: 'tanggal_lahir' },
            { data: 'jenis_kelamin', name: 'jenis_kelamin' },
            { data: 'actions', name: 'actions' }
        ]
    });
});
</script>
@endpush