@extends('adminlte::page')

@section('title', 'Create Pasien')

@section('content_header')
    <h1>Create Pasien</h1>
@stop

@section('content')
<form action="{{ route('pasien.store') }}" method="post">
    {!! csrf_field() !!}

    <div class="form-group has-feedback {{ $errors->has('nama') ? 'has-error' : '' }}">
        <label for="nama">Nama</label>
        <input type="text" name="nama" class="form-control" value="{{ old('nama') }}">
        @if ($errors->has('nama'))
            <span class="help-block">
                <strong>{{ $errors->first('nama') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('alamat') ? 'has-error' : '' }}">
        <label for="alamat">Alamat</label>
        <textarea name="alamat" id="alamat" class="form-control">{{ old('alamat') }}</textarea>
        @if ($errors->has('alamat'))
            <span class="help-block">
                <strong>{{ $errors->first('alamat') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('no_telp') ? 'has-error' : '' }}">
        <label for="no_telp">No Telepon</label>
        <input type="text" name="no_telp" class="form-control" value="{{ old('no_telp') }}">
        @if ($errors->has('no_telp'))
            <span class="help-block">
                <strong>{{ $errors->first('no_telp') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('rt_rw') ? 'has-error' : '' }}">
        <label for="rt_rw">RT/RW</label>
        <input type="text" name="rt_rw" class="form-control" value="{{ old('rt_rw') }}">
        @if ($errors->has('rt_rw'))
            <span class="help-block">
                <strong>{{ $errors->first('rt_rw') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('kelurahan_id') ? 'has-error' : '' }}">
        <label for="kelurahan_id">Kelurahan</label>
        <select name="kelurahan_id" id="kelurahan_id" class="form-control">
            @if ($kelurahan->count() > 0)
                @foreach ($kelurahan as $value)
                    <option value="{{ $value->id }}">{{ $value->kelurahan.' - '.$value->kecamatan.', '.$value->kota }}</option>
                @endforeach
            @else
                <option value="0">no data</option>
            @endif
        </select>
        @if ($errors->has('kelurahan_id'))
            <span class="help-block">
                <strong>{{ $errors->first('kelurahan_id') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('tanggal_lahir') ? 'has-error' : '' }}">
        <label for="tanggal_lahir">Tanggal Lahir</label>
        <input type="date" name="tanggal_lahir" class="form-control" value="{{ old('tanggal_lahir') }}">
        @if ($errors->has('tanggal_lahir'))
            <span class="help-block">
                <strong>{{ $errors->first('tanggal_lahir') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('jenis_kelamin') ? 'has-error' : '' }}">
        <label for="jenis_kelamin">Jenis Kelamin</label>
        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
            <option value="Pria">Pria</option>
            <option value="Wanita">Wanita</option>
        </select>
        @if ($errors->has('jenis_kelamin'))
            <span class="help-block">
                <strong>{{ $errors->first('jenis_kelamin') }}</strong>
            </span>
        @endif
    </div>
    <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop