@extends('adminlte::page')

@section('title', 'Kelurahan')

@section('content_header')
    <h1>Data Kelurahan</h1>
@stop

@section('content')
@can('user-admin')
  <a href="{{ route('kelurahan.create') }}" class="btn btn-primary btn-md">Create Kelurahan</a>
@endcan
<table class="table table-striped table-hover table-bordered" id="kelurahan-table">
  <thead>
    <tr>
      <th>ID</th>
      <th>Kelurahan</th>
      <th>Kecamatan</th>
      <th>Kota</th>
      <th>Created At</th>
      <th></th>
    </tr>
  </thead>
</table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@push('js')
<script>
$(function() {
    $('#kelurahan-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.kelurahan') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'kelurahan', name: 'kelurahan' },
            { data: 'kecamatan', name: 'kecamatan' },
            { data: 'kota', name: 'kota' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' }
        ]
    });
});
</script>
@endpush