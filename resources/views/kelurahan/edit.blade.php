@extends('adminlte::page')

@section('title', 'Edit Kelurahan')

@section('content_header')
    <h1>Edit Kelurahan</h1>
@stop

@section('content')
<form action="{{ route('kelurahan.update', $kelurahan->id) }}" method="post">
    {!! csrf_field() !!}
    <input type="hidden" name="_method" value="put">
    
    <div class="form-group has-feedback {{ $errors->has('kelurahan') ? 'has-error' : '' }}">
        <input type="text" name="kelurahan" class="form-control" value="{{ $kelurahan->kelurahan }}">
        @if ($errors->has('kelurahan'))
            <span class="help-block">
                <strong>{{ $errors->first('kelurahan') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('kecamatan') ? 'has-error' : '' }}">
        <input type="text" name="kecamatan" class="form-control" value="{{ $kelurahan->kecamatan }}">
        @if ($errors->has('kecamatan'))
            <span class="help-block">
                <strong>{{ $errors->first('kecamatan') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('kota') ? 'has-error' : '' }}">
        <input type="text" name="kota" class="form-control" value="{{ $kelurahan->kota }}">
        @if ($errors->has('kota'))
            <span class="help-block">
                <strong>{{ $errors->first('kota') }}</strong>
            </span>
        @endif
    </div>
    <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop