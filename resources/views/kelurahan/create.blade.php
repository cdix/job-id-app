@extends('adminlte::page')

@section('title', 'Create Kelurahan')

@section('content_header')
    <h1>Create Kelurahan</h1>
@stop

@section('content')
<form action="{{ route('kelurahan.store') }}" method="post">
    {!! csrf_field() !!}

    <div class="form-group has-feedback {{ $errors->has('kelurahan') ? 'has-error' : '' }}">
        <label for="kelurahan">Kelurahan</label>
        <input type="text" name="kelurahan" class="form-control" value="{{ old('kelurahan') }}">
        @if ($errors->has('kelurahan'))
            <span class="help-block">
                <strong>{{ $errors->first('kelurahan') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('kecamatan') ? 'has-error' : '' }}">
        <label for="kecamatan">Kecamatan</label>
        <input type="text" name="kecamatan" class="form-control" value="{{ old('kecamatan') }}">
        @if ($errors->has('kecamatan'))
            <span class="help-block">
                <strong>{{ $errors->first('kecamatan') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('kota') ? 'has-error' : '' }}">
        <label for="kota">Kota</label>
        <input type="text" name="kota" class="form-control" value="{{ old('kota') }}">
        @if ($errors->has('kota'))
            <span class="help-block">
                <strong>{{ $errors->first('kota') }}</strong>
            </span>
        @endif
    </div>
    <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop