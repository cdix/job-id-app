<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
  return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin', 'before' => 'admin'], function () {
  Route::get('/', 'HomeController@index');
  Route::resource('/kelurahan', 'KelurahanController');
  Route::resource('/pasien', 'PasienController');
  Route::resource('/users', 'UsersController');
});

Route::group(['prefix' => 'datatables', 'before' => 'datatables'], function () {
  Route::get('/', ['as' => 'datatables.index', 'uses' => 'DatatablesController@index']);
  Route::get('users', ['as' => 'datatables.users', 'uses' => 'DatatablesController@users']);
  Route::get('kelurahan', ['as' => 'datatables.kelurahan', 'uses' => 'DatatablesController@kelurahan']);
  Route::get('pasien', ['as' => 'datatables.pasien', 'uses' => 'DatatablesController@pasien']);
});
