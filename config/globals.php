<?php
function postLink($url, $label, $message = '', $title = '', $verb = 'DELETE')
{
  $key = md5(microtime());
  echo "<form id='$key' method='post' style='display:none;' name='$key' action='$url'>";
  echo "<input type='hidden' name='_token' value='" . csrf_token() . "'>";
  echo "<input type='hidden' name='_method' value='$verb'>";
  echo "</form>";
  echo "<a onclick='if (confirm(\"" . ($message != '' ? $message : 'Anda yakin menghapus data ini?') . "\")) { $(this).prev(\"form\").submit(); } event.returnValue = false; return false;' title='" . ($title != "" ? $title : "Hapus data ini") . "' href='#'>";
  echo $label;
  echo "</a>";
}
